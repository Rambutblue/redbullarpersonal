using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(RectTransform)),RequireComponent(typeof(Image))]
public class PointerScript : MonoBehaviour
{
    [SerializeField]
    private GameObject Target;
    [SerializeField]
    private float borderSize = 100;

    RectTransform rt;
    Image image;
    bool isBehindCam;
    Vector3 screenMidpoint;

    void Start()
    {
        rt = GetComponent<RectTransform>();
        image = GetComponent<Image>();
        screenMidpoint = new Vector3 (Screen.width / 2, Screen.height / 2, 0);
    }

    void Update()
    {
        Vector3 objScreenPos = Camera.main.WorldToScreenPoint(Target.transform.position);
        isBehindCam = Camera.main.transform.InverseTransformPoint(Target.transform.position).z < 0;
        
        Vector3 dir;
        if (isBehindCam)
        {
            dir = (rt.position - objScreenPos).normalized;
        }
        else
        {
            dir = (objScreenPos - rt.position).normalized;
        }

        //pointer direction
        float angle = Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x);
        rt.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


        bool isOffScreen = objScreenPos.x <= borderSize || objScreenPos.x >= Screen.width - borderSize || objScreenPos.y <= borderSize || objScreenPos.y >= Screen.height - borderSize;
        image.enabled = isOffScreen && GameManager.instance.isPlaneFlying;

        //pointer position
        if (isOffScreen)
        {
            Vector3 cappedPos = objScreenPos;
            if (cappedPos.x <= borderSize)
            {
                cappedPos.x = borderSize;
            }  
            if (cappedPos.x >= Screen.width - borderSize)
            {
                cappedPos.x = Screen.width - borderSize;
            }   
            if (cappedPos.y <= borderSize)
            {
                cappedPos.y = borderSize;
            }  
            if (cappedPos.y >= Screen.height - borderSize)
            {
                cappedPos.y = Screen.height - borderSize;
            }

            if (isBehindCam)
            {
                Vector3 screenPosDiff = cappedPos - screenMidpoint;
                Vector3 inversedCappedPos = new Vector3(-screenPosDiff.x, -screenPosDiff.y, 0);
                cappedPos = screenMidpoint + inversedCappedPos;
            }
            rt.position = cappedPos;
        }
    }
}
