using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtonManager : MonoBehaviour
{
    [SerializeField]
    private GameObject DefaultUI;
    [SerializeField]
    private GameObject HowToPlay;

    public void Play()
    {
        SceneManager.LoadScene("Main");
    }

    public void DefaultHTPSwitch()
    {
        DefaultUI.SetActive(!DefaultUI.activeInHierarchy);
        HowToPlay.SetActive(!HowToPlay.activeInHierarchy);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
