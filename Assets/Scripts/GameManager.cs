using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; private set; }

    public bool isStandOnScene = false;
    public bool isPlaneFlying = false;

    public bool isPitchLocked = true;
    public bool isPitchInversed = false;

    public bool isPlacingPlane = false;


    private int _obstacleCount;
    public int obstacleCount
    {
        get { return _obstacleCount; }
        set
        {
            _obstacleCount = value;
        }
    }
    public int currentTargetObstacle;

    private bool isGameOver;


    private float flightTime;
    private float currentScore;
    private float bestScore;

    private Vector3 basePlanePosition;
    private Quaternion basePlaneRotation;

    [SerializeField]
    private GameObject plane;
    [SerializeField]
    private GameObject stand;
    [SerializeField]
    private PlaneRollConrol planeRoll;
    [SerializeField]
    private PlaneFlight planeFlight;
    [SerializeField]
    private GameObject joystick;

    [SerializeField]
    private TextMeshProUGUI currentScoreText;
    [SerializeField]
    private TextMeshProUGUI bestScoreText;

    [SerializeField]
    private GameObject readyButton;
    [SerializeField]
    private GameObject flyButton;
    [SerializeField]
    private GameObject gameOverButtons;
    [SerializeField]
    private GameObject scoreboard;

    private bool _isJoystickActive;
    private bool isJoystickActive
    {
        get { return _isJoystickActive; }
        set 
        { 
            _isJoystickActive = value;
            joystick.SetActive(_isJoystickActive);
        }
    }

    // Start is called before the first frame update
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
        isJoystickActive = false;
        isGameOver = false;
    }


    private void Update()
    {
        if (!isGameOver && isPlaneFlying)
        {
            flightTime += Time.deltaTime;
            currentScore = Mathf.Round(flightTime * 100) / 100;
            currentScoreText.text = "Your time: " + currentScore + " s";
        }
    }

    public void StartFlight()
    {
        if (isStandOnScene)
        {
            planeFlight.enabled = true;
            planeRoll.enabled = true;
            currentTargetObstacle = 1;
            isJoystickActive = true;
            isPlaneFlying = true;

            
            stand.SetActive(false);
            scoreboard.SetActive(true);
            flyButton.SetActive(false);
        }       
    }

    public void ReadyButton()
    {
        if(obstacleCount > 0)
        {
            isPlacingPlane = true;
            readyButton.SetActive(false);
        }
    }

    public void TryAgain()
    {
        plane.transform.position = basePlanePosition;
        plane.transform.rotation = basePlaneRotation;

        planeFlight.enabled = false;
        planeRoll.enabled = false;
        currentTargetObstacle = 0;
        isJoystickActive = false;
        isPlaneFlying = false;
        stand.SetActive(true);

        gameOverButtons.SetActive(false);
        flyButton.SetActive(true);

        flightTime = 0;
        isGameOver = false;


    }

    

    public void GameEndCheck()
    {
        if (currentTargetObstacle > obstacleCount)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        isGameOver = true;
        if (currentScore < bestScore || bestScore == 0)
        {
            bestScore = currentScore;
            bestScoreText.text = "Best time: " + bestScore + " s";
        }
        gameOverButtons.SetActive(true);
        
    }

    public void AfterPlanePlacedSetup()
    {
        isStandOnScene = true;
        flyButton.SetActive(true);
        basePlanePosition = plane.transform.position;
        basePlaneRotation = plane.transform.rotation;
    }
}
