using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.EventSystems;
using TMPro;

public class ARCursor : MonoBehaviour
{
    [SerializeField]
    private GameObject[] placedObjectCursor; 
    [SerializeField]
    private GameObject[] placedObj;
    [SerializeField]
    private string[] placedObjectsDescription;
    [SerializeField]
    private ARRaycastManager raycastManager;
    [SerializeField]
    private GameObject planeStand;
    [SerializeField]
    private GameObject planeStandCursor;
    [SerializeField]
    private GameObject ARAnchorPrefab;
    [SerializeField]
    private TextMeshProUGUI currentObstacleText;


    bool foundHorizontalPlane = false;
    int obstacleType = 0;

    private void Start()
    {
        currentObstacleText.text = placedObjectsDescription[obstacleType];
    }

    void Update()
    {
        UpdateCursor();

        if (GameManager.instance.isPlacingPlane)
        {
            ChangeToPlanePlacement();
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && foundHorizontalPlane && !EventSystem.current.IsPointerOverGameObject(0))
        {
            if (!GameManager.instance.isPlacingPlane)
            {
                GameManager.instance.obstacleCount++;
                GameObject ARAnchor = Instantiate(ARAnchorPrefab, transform.position, transform.rotation);
                GameObject currentObstacle = Instantiate(placedObj[obstacleType], transform.position, transform.rotation); 
                currentObstacle.transform.SetParent(ARAnchor.transform);

            }
            else if (GameManager.instance.isPlacingPlane)
            {
                planeStand.transform.position = transform.position;
                planeStand.transform.rotation = transform.rotation;
                planeStand.SetActive(true);
                planeStandCursor.SetActive(false);
                GameManager.instance.AfterPlanePlacedSetup();



                gameObject.SetActive(false);
            }

        }



    }

    void UpdateCursor()
    {
        Vector2 screenPosition = Camera.main.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        raycastManager.Raycast(screenPosition, hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);

        if (hits.Count > 0)
        {
            placedObjectCursor[obstacleType].SetActive(true);
            foundHorizontalPlane = true;
            transform.position = hits[0].pose.position;
            transform.rotation = hits[0].pose.rotation;
        }
    }
    void ChangeToPlanePlacement()
    {
        placedObjectCursor[obstacleType].SetActive(false);
        planeStandCursor.SetActive(true);
    }

    public void PlusObstacleType() 
    {
        placedObjectCursor[obstacleType].SetActive(false);
        if (obstacleType + 1 < placedObj.Length)
        {
            obstacleType++;
        }
        else
        {
            obstacleType = 0;
        }
        placedObjectCursor[obstacleType].SetActive(true);
        currentObstacleText.text = placedObjectsDescription[obstacleType];
    }

    public void MinusObstacleType() 
    {
        placedObjectCursor[obstacleType].SetActive(false);
        if (obstacleType == 0)
        {
            obstacleType = placedObj.Length - 1;
        }
        else
        {
            obstacleType--;
        }
        placedObjectCursor[obstacleType].SetActive(true);
        currentObstacleText.text = placedObjectsDescription[obstacleType];
    }


}


























/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARCursor : MonoBehaviour
{
    public GameManager gameManager;

    public GameObject cursorChildObstacle;
    public GameObject cursorChildPlaneStand;
    public GameObject planeStand;
    public GameObject placedObj;
    public ARRaycastManager raycastManager;

    private bool useCursor = true;
    private int obstacleCount
    {
        get { return this.obstacleCount; }
        set  
        { 
            this.obstacleCount = value;
            if (value > 3)
            {
                cursorChildObstacle.SetActive(false);
                cursorChildPlaneStand.SetActive(true);
                gameManager.isStandOnScene = true;
            }
            if (value > 4)
            {
                planeStand.SetActive(false);
            }
        }
    }

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCursor();

        

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (useCursor)
            {
                if (obstacleCount < 3)
                {
                    Instantiate(placedObj, transform.position, transform.rotation);
                    obstacleCount++;
                }
                else if (obstacleCount == 3)
                {
                    planeStand.transform.position = transform.position;
                    planeStand.transform.rotation = transform.rotation;
                    planeStand.SetActive(true);
                    obstacleCount++;
                }
            }
            else
            {
                List<ARRaycastHit> hits = new List<ARRaycastHit>();
                raycastManager.Raycast(Input.GetTouch(0).position, hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);
                if (hits.Count > 0)
                {
                    Instantiate(placedObj, hits[0].pose.position, hits[0].pose.rotation);
                }
            }

        }
    }

    void UpdateCursor()
    {
        Vector2 screenPosition = Camera.main.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        raycastManager.Raycast(screenPosition, hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);

        if (hits.Count > 0)
        {
            transform.position = hits[0].pose.position;
            transform.rotation = hits[0].pose.rotation;
        }

    }
    void ChangeCursor()
    {
        cursorChildObstacle.SetActive(false);
        cursorChildPlaneStand.SetActive(true);

    }
}
*/