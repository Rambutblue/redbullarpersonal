using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneRollConrol : MonoBehaviour
{
    [SerializeField]
    private FixedJoystick joystick;
    [SerializeField]
    private Transform noRollTransform;
    [SerializeField]
    private float rollSpeed = 4;
    [SerializeField]
    private float currentRollSpeed;

    private Vector3 joystickInputRoll;

    private bool isInTrickRoll = false;
    private float trickRollSpeed = 300;
    private float trickRollSmoothness = 3;

    // Update is called once per frame
    void Update()
    {
        if(joystick.Direction.y < -0.15f && joystick.Direction.y > -0.35f)
        {
            currentRollSpeed = rollSpeed / 8;
        }
        else
        {
            currentRollSpeed = rollSpeed;
        }


        if (!isInTrickRoll)
        {
            if (joystick.Direction.y < -0.25f)
            {
                joystickInputRoll = new Vector3(-joystick.Direction.x, -joystick.Direction.y, 0);
            }
            else
            {
                joystickInputRoll = new Vector3(joystick.Direction.x, joystick.Direction.y, 0);
            }

            
            
            Vector3 joystickInputRollWorld = noRollTransform.TransformDirection(joystickInputRoll);
            Quaternion desiredRoll = Quaternion.LookRotation(transform.forward, joystickInputRollWorld);
            
            transform.rotation = Quaternion.Lerp(transform.rotation, desiredRoll, rollSpeed * Time.deltaTime);

            
        }
        
    }

    /*
    private IEnumerator TrickRoll()
    {
        isInTrickRoll = true;
        float rollAngle = 0;

        while (rollAngle < 360)
        {
            
            Vector3 trickRollInput = Vector3.right;
            
            Quaternion vectorRoll = Quaternion.Euler(0, 0, -rollAngle);
            trickRollInput = vectorRoll * trickRollInput;
            Debug.Log(trickRollInput);
            Vector3 trickRollWorld = noRollTransform.TransformDirection(trickRollInput);
            Quaternion desiredTrickRoll = Quaternion.LookRotation(transform.forward, trickRollWorld);
            transform.rotation = Quaternion.Lerp(transform.rotation, desiredTrickRoll, trickRollSmoothness * Time.deltaTime);
            rollAngle += trickRollSpeed * Time.deltaTime;
            yield return null;
        }

        
        isInTrickRoll = false;
    }

    public void StartTrickRoll()
    {
        if (!isInTrickRoll)
        {
            StartCoroutine(TrickRoll());
        }       
    }
    */
}
