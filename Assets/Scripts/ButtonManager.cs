using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    [SerializeField]
    private GameObject settings;
    public void PitchLock()
    {
        GameManager.instance.isPitchLocked = !GameManager.instance.isPitchLocked;
    }
    public void PitchInverse()
    {
        GameManager.instance.isPitchInversed = !GameManager.instance.isPitchInversed;
    }
    public void SettingsToggle()
    {
        settings.SetActive(!settings.activeInHierarchy);
        if (settings.activeInHierarchy)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1; 
        }
    }
    public void ResetTrack()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
