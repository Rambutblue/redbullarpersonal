using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlaneFlight : MonoBehaviour
{
    [SerializeField]
    private float speed = 3;
    private float _speed;
    [SerializeField]
    private FixedJoystick joystick;
    [SerializeField]
    private float pitchSpeed = 40;
    [SerializeField]
    private float pitchRotationSpeed = 3;
    [SerializeField]
    private float maxPitchAngle = 30;

    [SerializeField]
    private float speedBoostMultiplier = 2.3f;
    [SerializeField]
    private float rampUpTime = 0.3f;
    [SerializeField]
    private float boostDuration = 1.5f;


    private bool isPitchUpLocked = false;
    private bool isPitchDownLocked = false;
    private float lastFrameY;


    private float boostedSpeed;
    private float boostedSpeedDiff;
    private bool isBoostActive = false;

    // Start is called before the first frame update
    void Start()
    {
        _speed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (true)
        {
            //joystick input
            float xInput = joystick.smoothDirection.x;
            float yInput = joystick.smoothDirection.y;

            if(GameManager.instance.isPitchInversed)
            {
                yInput = -yInput;
            }

            //plane movement
            transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);

            //plane left/right rot
            transform.Rotate(Vector3.up * xInput * pitchSpeed * Time.deltaTime);



            //plane up/down rot
            Vector3 flatOrientation = transform.forward;
            flatOrientation.y = 0;
            Quaternion flatRotation = Quaternion.LookRotation(flatOrientation);
            if (GameManager.instance.isPitchLocked)
            {
                isPitchUpLocked = yInput > 0 && Quaternion.Angle(flatRotation, transform.rotation) > maxPitchAngle && lastFrameY < yInput;
                isPitchDownLocked = yInput < 0 && Quaternion.Angle(flatRotation, transform.rotation) > maxPitchAngle && lastFrameY > yInput;

                if (!isPitchUpLocked && yInput > 0)
                {
                    transform.Rotate(Vector3.right * -yInput * pitchSpeed * Time.deltaTime);
                }
                if (!isPitchDownLocked && yInput < 0)
                {
                    transform.Rotate(Vector3.right * -yInput * pitchSpeed * Time.deltaTime);
                }
            }
            else
            {
                transform.Rotate(Vector3.right * -yInput * pitchSpeed * Time.deltaTime);
            }

            
            
            //default plane rot
            if (joystick.Horizontal == 0 && joystick.Vertical == 0)
            {
                
                transform.rotation = Quaternion.Lerp(transform.rotation, flatRotation, pitchRotationSpeed * Time.deltaTime);
            }

            

            lastFrameY = transform.position.y;
        }

    }
    private void OnCollisionEnter(Collision collision) 
    {
        if (collision.gameObject.CompareTag("SpeedObstacle"))
        {
            BoostStart();
        }
    }
    private IEnumerator SpeedControl() 
    {
        isBoostActive = true;
        boostedSpeed = _speed * speedBoostMultiplier;
        boostedSpeedDiff = boostedSpeed - _speed;
        while (speed < boostedSpeed)
        {
            speed += (boostedSpeedDiff / rampUpTime) * Time.deltaTime;
            yield return null;
        }
        while (speed > _speed)
        {
            speed -= (boostedSpeedDiff / boostDuration) * Time.deltaTime;
            yield return null;
        }
        speed = _speed;
        isBoostActive = false;
        
    }

    private void BoostStart()
    {
        if (!isBoostActive)
        {
            StartCoroutine(SpeedControl());
        }
        else
        {
            StopAllCoroutines();
            speed = _speed;
            StartCoroutine(SpeedControl());
        }
    }

    public void TestBoost()
    {
        StartCoroutine(SpeedControl());
    }
}
