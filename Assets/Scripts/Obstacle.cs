using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField]
    private GameObject greenMark;

    private int obstacleNum;
    void Start()
    {
        obstacleNum = GameManager.instance.obstacleCount;
    }

    void Update()
    {
        if (GameManager.instance.currentTargetObstacle == obstacleNum)
        {
            greenMark.SetActive(true);
        }
        else
        {
            greenMark.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && GameManager.instance.currentTargetObstacle == obstacleNum)
        {
            GameManager.instance.currentTargetObstacle++;
        }
        GameManager.instance.GameEndCheck();   
    }
}
